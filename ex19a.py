# Exercise 19: Functions and Variables
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-26

def how_many_cookies_in_the_jar(number):
    print(f"There are currently {number} of cookies in the jar.")


cookies = 0
how_many_cookies_in_the_jar(cookies)

answer = input("Do you want to add some cookies? ")
cookies = cookies + int(answer)
how_many_cookies_in_the_jar(cookies)

print("Adding 2 cookies from your mum.")
how_many_cookies_in_the_jar(cookies + 2)

print("But this is not how it works. We need to add them to the cookies")
cookies = cookies + 2
how_many_cookies_in_the_jar(cookies)

answer = input("Do you want to add more cookies? ")
cookies = cookies + int(answer)
how_many_cookies_in_the_jar(cookies)

print("Your brother came by and took 5 cookies!")
cookies = cookies - 5
how_many_cookies_in_the_jar(cookies)

print(f"Mum takes all the {cookies} cookies with her.")
cookies = 0
how_many_cookies_in_the_jar(cookies)
