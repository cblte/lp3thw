# Exercise 13: Parameters, Unpacking, Variables
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-18

from sys import argv
# read the WYSS section for how to run this
script, first, second, third, fourth = argv

print("The script is called:...... ", script)
print("Your first parameter is :.. ", first)
print("Your second parameter is :. ", second)
print("Your third parameter is :.. ", third)
print("Your fourth parameter is :. ", fourth)