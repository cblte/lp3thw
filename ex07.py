# Exercise 7: More Printing
# https://learnpythonthehardway.org/python3/ex7.html
# cblte 2020-10-09

# create some print statements
print("Mary had a little lamb.")
# here we use a combination of placeholder and format
print("It's fleece was white as {}.".format("show"))
print("And everywhere that Mary went.")
# print out 10 dots
print("." * 10) 

# define a bunch of variables to print 'Cheese Burger'
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that comma at the end, try removing it to see whats happens
# with the 'end' we can add extra characters to the print
print(end1 + end2 + end3 + end4 + end5 + end6, end=' ')
print(end7 + end8 + end9 + end10 + end11 + end12)

