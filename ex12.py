# Exercise 12: Prompting People
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-17


age = input("How old are you? ")
height = input("How tall are you? ")
weight = input("How much do you weigh? ")

print(f"So, you're {age} years old, you are {height} tall and you weigh {weight}")
