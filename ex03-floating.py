# Exercise 3: Numbers and Math
# https://learnpythonthehardway.org/python3/ex3.html
# cblte 2020-10-05

# prints text
print("I will now count my chickens:")

# prints text plus the result of the equation
print("Hens", 25 + 30 / 6.0)
print("Roosters", 100 - 25 * 3 % 4.0)

# prints text
print("Now I will count the eggs:")

# does a equation and prints result.
print(3 + 2 + 1 - 5 + 4 % 2.0 - 1 / 4.0 + 6)

# prints text
print("Is it true that 3 + 2 < 5 - 7?")

# prints the result of the equation
print(3 + 2 < 5 - 7)

# prints text and the result of the equation
print("What is 3 + 2?", 3 + 2)
print("What is 5 - 7?", 5 - 7)

# prints text
print("Oh, that's why it's False.")

# prints text
print("How about some more.")

# prints text and the result of the equation
print("It is greater?", 5 > -2)
print("Is it greater or equal?", 5 >= -1)
print("Is it less or equal?", 5 <= -2)
