# Exercise 13: Parameters, Unpacking, Variables
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-18

from sys import argv
# read the WYSS section for how to run this
script, name = argv

print("The script is called:...... ", script)
age = input(f"Hello {name}, how old are you? ")
print(f"So {name}, you are {age} years old. Nice.")

