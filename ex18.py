# Exercise 18: Names, Variables, Code, Functions
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-24

# this exercise will introduce functions

# this one is like your scripts with argv
def print_two(*args):
    arg1, arg2 = args
    print(f"arg1: {arg1}, arg2: {arg2}")

# ok, that *args is actually pointless, we can just do this
def print_two_again(arg1, arg2):
    print(f"arg1: {arg1}, arg2: {arg2}")

# thus just takes one argument
def print_one(arg1):
    print(f"arg1: {arg1}")

# this one takes no arguments
def print_none():
    print("I got nothing.")

print_two("Carsten", "Brüggenolte")
print_two_again("Carsten", "Brüggenolte")
print_one("Carsten")
print_none()

