# Exercise 19: Functions and Variables
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-26

# define a function for print cheese and crackers
def cheese_and_crackers(cheese_count, boxes_or_crackers):
    print(f"You have {cheese_count} cheeses.")
    print(f"You have {boxes_or_crackers} boxes of crackers")
    print("Man, that's a enough for a party.")
    print("Get a blanket.\n")

# call function with numbers a arguments
print("We can just give the function numbers directly:")
cheese_and_crackers(29, 30)

# define variables
print("OR, we can use variables from our script.")
amount_of_cheese = 10
amount_or_crackers = 50
# call function with the variables instead of numbers
cheese_and_crackers(amount_of_cheese, amount_or_crackers)

# do some math inside the function call. the expression is handled
# before the function is called. so the function will receive the
# result of the expressions.
print("Wen can even do math inside too:")
cheese_and_crackers(10 + 20, 5 + 6)

# so some more math this time with variables inside the argument part
print("And we can combine the two, variables and math:")
cheese_and_crackers(amount_or_crackers + 100, amount_or_crackers + 1000)

