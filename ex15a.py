# Exercise 15: Reading files
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-21

# read in a file with
# filename comes in as a parameter

# import from sys package the argv module
from sys import argv

script, filename = argv

# open the file and assign it to a variable
txt = open(filename)

# print out the filename
print(f"Here's your file {filename}")
# read the contents of the file and print them out
print(txt.read())
txt.close()