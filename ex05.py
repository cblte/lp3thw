# Exercise 5: More Variables and Printing
# https://learnpythonthehardway.org/python3/ex5.html
# cblte 2020-10-09

name = 'Carsten Brueggenolte'
age = 40 # not a lie
height = 189
weight = 105
eyes = 'Grey'
teeth = 'white'
hair = 'brown'

print(f"Let's talk about {name}.")
print(f"He's {height} cms tall.")
print(f"He's {weight} kilogram heavy.")
print(f"Actually that's a little bit too heavy.")
print(f"He's got {eyes} eyes and {hair} hair.")
print(f"His teeth are usually {teeth} depending on the coffee.")

weight_in_pounds = weight * 2.20
print(f"His weight {weight}(kg) in pounds is {round(weight_in_pounds,2)}")
height_in_inch = height / 2.54
print(f"His height {height}(cm) in inch is {round(height_in_inch,2)}")

# this line is tricky, try to get it exactly right
total = age + height + weight
print(f"If I add {age}, {height}, and {weight} I get {total}")
