# Exercise 16: Reading and writing files
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-21

# readig the file we creates in ex16.py

# importing modules
from sys import argv

# unpacking program arguments
script, filename = argv

print(f"Reading the {filename} now:\n")

file = open(filename)
print(file.read())

file.close()