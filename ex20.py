# Exercise 20: Functions and Files
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-26

from sys import argv

# unpack stuff
script, input_file = argv

# defining some functions

def print_all(f):
    print(f.read())

def rewind(f):
    f.seek(0)

def print_a_line(line_count, f):
    # prints the number and 
    # one line from the file from where the cursor in the files is
    print(line_count, f.readline())

# open file passed via argument
current_file = open(input_file)

# pass the file the function to print all of it
print("First let's print the while file:\n")
print_all(current_file)

# set the cursor back to the beginning of the file
print("Now let's rewind, kind of like a tape.")
rewind(current_file)

# print one line after the other
print("Let's print three lines:")

# use a variable to count the lines
# currentline is somehow the line counter, since we do not 
# reset the cursor position and so we read line by 
# line from the file
current_line = 1
print_a_line(current_line, current_file)

current_line = current_line + 1
print_a_line(current_line, current_file)

current_line = current_line + 1
print_a_line(current_line, current_file)

