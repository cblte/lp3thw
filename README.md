# Learn-Python3-the-Hard-Way

![](images/learn-python3-the-hard-way.jpg)

These are all my answer for all exercises from Learn Python 3 the Hard Way: A Very Simple Introduction to the Terrifyingly Beautiful World of Computers and Code, First Edition by Zed. A. Shaw

Some chapters of the book can be read online https://learnpythonthehardway.org/python3/


## Chapters finished

- Preface
- The Hard Was Is Easier
- The Setup
- ex01: A Good First Program
- ex02: Comments and Pound Characters
- ex03: Numbers and Math
- ex04: Variables and Names
- ex05: More Variables and Printing
- ex06: Strings and Text
- ex07: More Printing
- ex08: Printing, Printing
- ex09: Printing, Printing, Printing
- ex10: What Was That?
- ex11: Asking Questions
- ex12: Prompting People
- ex13: Parameters, Unpacking, Variables
- ex14: Prompting and Passing
- ex15: Reading files
- ex16: Reading and writing files
- ex17: More files
- ex18: Names, Variables, Code, Functions
- ex19: Functions and Variables
- ex20: Functions and Files



