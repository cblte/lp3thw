# Exercise 21: Functions can return something
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-27

def add(a, b):
    """add a and b and return result
    """
    print(f"adding {a} + {b}")
    return a + b

def subtract(a, b):
    """subtract b from a and return result
    """
    print(f"subtracting {a} - {b}")
    return a - b

def multiply(a, b):
    """Multiply a and b and return the result
    """
    print(f"multiply {a} * {b}")
    return a * b

def devide(a, b):
    """a is devided by b and return the result
    """
    print(f"devide {a} / {b}")
    return a / b


print("Let's do some math with our functions.")

age = add(30, 5)
height = subtract(78,4)
weight = multiply(90, 2)
iq = devide(100, 2)

print(f"Age: {age}, Height: {height}, Weight: {weight}, IQ: {iq}")

# A puzzle for the extra credit, type it in anyway

print("Here is a puzzle.")


what = add(age, subtract(height, multiply( weight, devide(iq,2))))

# doing it by hand 
# 50/2= 25 / 
# 180 * 35 = 4500 / 
# 74 - 4500 = -4426 / 
# 35 + - 4426 = 4391
#              ====== 

print("That becomes: ", what, "Can you do it by hand?")


# ------------------------
# Study Drill 
# Formula: 24 + 34 / 100 - 1023
print("Study Drill Example: ")
result = subtract(add(24, devide(34, 100)), 1023)
print(f"THe result of the Study Drill is: {result}")