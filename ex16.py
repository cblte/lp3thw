# Exercise 16: Reading and writing files
# https://learnpythonthehardway.org/python3/
# cblte 2020-10-21

# write some simple text editor

# importing modules
from sys import argv

# unpacking program arguments
script, filename = argv

# printing introduction
print(f"We're going to erase {filename}.")
print("If you don't want that , hit CTRL+C (^C)")
print("If you want to erase that file, just press RETURN.")
input("?")

# opening the file in write ("w") mode and truncating it
print("Opening the file...")
target = open(filename, "w")
print("Truncating the file now.")
target.truncate()

# assking the user for some input
print("Now I'm going to as you for three lines.")
line1 = input ("line 1: ")
line2 = input ("line 2: ")
line3 = input ("line 3: ")

# writing the lines to the file. 
# with the "\n" we add a new line at the end of the file
print("I'm going to write these lines to the file.")
target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

# closing the file so it can bes used by other programs
print("And finally we will close the file.")
target.close()


