# Exercise 11: Asking Questions
# https://learnpythonthehardway.org/python3/ex11.html
# cblte 2020-10-17

print("How old are you?", end=' ')
age = input()
print("How tall are you?", end=' ')
height = input()
print("How much do you weigh?", end=' ')
weight = input()

print(f"So, your {age} old, {height} tall and {weight} heavy.")
