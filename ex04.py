# Exercise 4: Variables and Names
# https://learnpythonthehardway.org/python3/ex4.html
# cblte 2020-10-05

# How many cars do I have
cars = 100
# to keep the amount of seats in a car
space_in_a_car = 4.0
# ^^using only '4' will affect the multiplication
# the division will result in a float value

# how many drivers do we habe
drivers = 30
# how many passengers do we have
passengers = 90
# how many cars stand still
cars_not_driven = cars - drivers
# one driver per car only
cars_driven = drivers
# how many passengers could we transport
carpool_capacity = cars_driven * space_in_a_car
# how many passengers have we transported by average
average_passengers_per_car = passengers / cars_driven

print("There are", cars, "cars available.")
print("There are only", drivers, "drivers available.")
print("There will be", cars_not_driven, "empty cars today.")
print("We can transport", passengers, "people today.")
print("We have", carpool_capacity, "to carpool today.")
print("We need to put about", average_passengers_per_car, "in each car.")
