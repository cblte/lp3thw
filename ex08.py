# Exercise 8: Printing, Printing
# https://learnpythonthehardway.org/python3/ex8.html
# cblte 2020-10-10

# defining some sort of function. 
# using the .format on the variable formatter 
# will fill the {} with text passed in
formatter = "{} {} {} {}"

# print out 1, 2, 3, 4
print(formatter.format(1,2,3,4))
# print out "one two three four"
print(formatter.format("one", "two", "three", "four"))
# print out True False False False True
print(formatter.format(True, False, False, True))
# print out {} {} {} {} {} {} {} {} {} {} {} {} {} {} {} {}
print(formatter.format(formatter, formatter, formatter, formatter))
# print out the text. 
print(formatter.format(
    "Try your", 
    "Own Text here",
    "Maybe a poem",
    "Or a song about fear?"
  ))

