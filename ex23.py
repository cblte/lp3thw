# Exercise 23: Strings, Bytes and Character Encodings
# https://learnpythonthehardway.org/python3/
# cblte 2020-11-23

# working with UTF-8 encocded strings
# for this to work, we need the languages.txt file


import sys
script, encoding, error = sys.argv

# defining a hand main function
def main(language_file, encoding, errors):
    # read a line of our file, will return empty string at the end of the file
    line = language_file.readline()

    # line not empty, continue (testing the truth of a variable)
    if line:
        print_line(line, encoding, errors)
        return main(language_file, encoding, errors)
        # ^^ call print_line with the line and the encoding, 
        # then call main again with the file_handle - nothing changes

    # ----- end of main 

# handy print_line function to simplify code
def print_line(line, encoding, errors):
    # strips out newline stuff from the string
    next_line = line.strip() 
    raw_bytes = next_line.encode(encoding, errors=errors)
    cooked_string = raw_bytes.decode(encoding, errors=errors)

    print(raw_bytes, " <===> ", cooked_string)
    
    # ----- end of print_line

# ----- programm start here
# -----
# open the file for reading
languages = open("languages.txt", encoding="utf-8")
main(languages,encoding, error)
    

