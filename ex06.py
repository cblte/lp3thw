# Exercise 6: Strings and Text
# https://learnpythonthehardway.org/python3/ex6.html
# cblte 2020-10-09

# add variable with value of 10
types_of_people = 10
# save formatted text in variable
x = f"There are {types_of_people} types of people."

# define 2 variables
binary = "binary"
do_not = "don't"
# save formatted string in variable
y = f"Those how know {binary} and those who {do_not}"

# print out both variables
print(x)
print(y)

# print out variables with more text
print(f"I said: {x}")
print(f"I also said: '{y}'")

# define variable with boolean value False
hilarious = False
# define variable with an empty placeholder
joke_evaluation = "Isn't that joke so funny?! {}"

# print out variable and fill empty placeholder now with .format method
print(joke_evaluation.format(hilarious))

# define two more variables with some text
w = "This is the left side of..."
e = "a string with a right side."

# concatenate variables and print
print(w + e)
