# Exercise 10a: What Was That?
# https://learnpythonthehardway.org/python3/ex10.html
# cblte 2020-10-17

# Study Drill Number 3
# Combine escape sequences and format strings to create a more complex format.

list_formatter = "\t * {}\n"

item1 = list_formatter.format("Cat food")
item2 = list_formatter.format("Fishies")
item3 = list_formatter.format("Catnip")
item4 = list_formatter.format("Grass")


fat_cat = "I'll do a list:\n"
print(fat_cat + item1 + item2 + item3 + item4)